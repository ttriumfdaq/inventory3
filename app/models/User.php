<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
    protected $fillable = ['hashed_password', 'user_name'];

    public static $rules = ['user_name' => 'required|unique:users|max:50',
        'hashed_password' => 'required|min:6|max:30',
        'cpassword' => 'required|same:hashed_password'];

    public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public static $messages = "";
	protected $hidden = array('password', 'remember_token');

    public static function isValid($data){
        $validator = Validator::make($data,
            static::$rules
        );

        if($validator->passes()){
            return true;
        } else {
            static::$messages = $validator->messages();
            return false;
        }
    }

}
