<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 8/20/2015
 * Time: 3:57 PM
 */

class Change extends Eloquent {

    protected $table = 'changes';
    public $timestamps = false;
    protected $guarded = array('id');
    protected $fillable = array('item_id', 'user_id', 'field','old_value', 'new_value', 'created_on');
}