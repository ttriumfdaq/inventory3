<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/15/15
 * Time: 2:34 PM
 */

class Item extends Eloquent {

    public $timestamps = false;

    protected $table = 'items';

    protected $availableFields = [];

    protected $guarded = array('id', 'category_id', 'kind_id');

    public function getAttributes(){
        return $this->attributes;
    }

}