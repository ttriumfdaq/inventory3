<?php

require_once('routes/admin.php');
require_once('routes/public.php');
require_once('routes/partials.php');
//Route::when('*', 'csrf', ['post']);

Route::post('daqinv/lists/UpdateData', function(){
    $array = explode("|", $_REQUEST['id']);
    $item_id = $array[0];
    $column = $array[1];

    $item = Item::where('id', '=', $item_id)->firstOrFail();
    $item->$column = $_REQUEST['value'];
    $item->save();
    return $_REQUEST['value'];
});

Route::post('/selectCol', array('as' => 'testEditable', 'uses' => 'DataController@postEditable'));

Route::post('/home', array('as' => 'post-search', 'uses' => 'HomeController@search'));



Route::get('/daqinv/lists/{categories}', array('as' => 'get-child-category', 'uses' =>'HomeController@lists'));
Route::get('/daqinv', 'HomeController@index');

Route::get('/daqinv/details/{item}', array('as' => 'get-item-details', function($itemID){

    $item = Item::where('id', '=', $itemID)->firstOrFail();
    $itemColumnsAndValues = $item->getAttributes();
    return View::make('item-details')->with(array(
        'item' => $item,
        'itemColumnsAndValues' => $itemColumnsAndValues
    ));
}));


//Route::post('/home', array(
//    'as' => 'post-sign-in',
//    'uses' => 'SessionsController@create'
//));
Route::get('/login', 'SessionsController@create');
Route::get('/logout', 'SessionsController@destroy');

Route::group(['before'=>'auth'], function(){

    Route::post('profile/change-password', array(
        'as' => 'post-change-password',
        'uses' => 'ProfileController@postChangePassword'));
    Route::get('profile/change-password', array(
        'as' => 'change-password',
        'uses' => 'ProfileController@changePassword'
    ));
    Route::when('root/*', 'root');
    Route::get('root/view-all-categories', array('as' => 'view-all-categories', 'uses' => 'ConfigurationController@viewAllCategories'));
});
//******************************Restricted to Root User **********************************
Route::when('root/*', 'root');
Route::group(['before' => 'root'], function(){

    Route::get('/root/createnew', array(
        'as' => 'get-create-new-user', function(){
            return View::make('root/new-user');
        }));

    Route::post('/root/createnew',
        array(
            'as' => 'post-create-new-user',
            'uses' =>'ProfileController@createNew'
        )
    );

    Route::get('/root/view-all-users', function(){
        return View::make('root/view-all-users');
    });

    Route::get('/root/view-all-categories', function(){
        $categoryHeadings = Category::where('parent_id', '=', 1)->get();
        $categories1 = Category::where('parent_id', '=', 1)->get();
      //  return View::make('root/view-all-categories')->with('categoryHeadings', $categoryHeadings);
        return View::make('root/view-all-categories')->with(array(
            'categories1' => $categories1
        ));
    });

    Route::post('/root/view-all-categories', array(
        'as' => 'post-add-new-category',
        'uses' => 'ConfigurationController@addNewCategory'));

    Route::get('/root/configuration', array('as' => 'root-configuration', 'uses' => 'ConfigurationController@index'));

    Route::get('root/edit-user/{id}',
        array(
            'as' => 'edit-user',
            'uses' => 'ProfileController@editUser'
        )
    );

    Route::get('root/delete-user/{id}',
        array(
            'as' => 'delete-user',
            'uses' => 'ProfileController@deleteUser'
        )
    );

    Route::get('root/change_category_name/{id}',
        array(
            'as' => 'change-category-name',
            'uses' => 'ConfigurationController@changeCategory'
        )
    );
});

//****************************** End Restricted pages**********************************
Route::resource('sessions', 'SessionsController');
