<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 6/2/2015
 * Time: 8:49 PM
 */

Route::get('/contact', function(){
    return View::make('contact');
});

Route::post('/contact', array('as' => 'contact-admin-post', 'uses' => 'HomeController@contactAdmin'));