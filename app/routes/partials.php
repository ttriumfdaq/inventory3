<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 8/10/15
 * Time: 4:26 PM
 */

Route::get('/select-col/{category_id}', 'DataController@getSelectColumn');

Route::post('/daqinv/lists/{param}', array('as' => 'selectCategory', 'uses' => 'HomeController@selectCategory'));

Route::get('/testing/{id}', 'HomeController@getHiddenColumns');

Route::get('/table/{categoryId}', array('as' => 'showTable', 'uses' => 'DataController@showTable'));
