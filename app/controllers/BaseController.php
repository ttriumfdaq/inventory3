<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	protected $retiredSetting;

	protected $idArray = array("id", "category_id", "kind_id");

	public function removeId($array){
		//removes id, kind id and category id
		for($i = 0; $i < count($array); $i++){
			if( in_array($array[$i], $this->idArray)){
				unset($array[$i]);
			}
		}
		$new = array_values($array);

		return $new;
	}
	public function getHiddenColumns($categoryId){

		$displayed_columns = $this->getDisplayedColumns($categoryId);
		$hidden_columns = array();
		$columns = $this->removeEmptyColumns($categoryId);

		//If not in displayed columns, push to hidden columns
		for ($i=0; $i < count($columns); $i++) {
			if(!in_array($columns[$i], $displayed_columns)){
				array_push($hidden_columns, $columns[$i]);
			}
		}
		return $hidden_columns;
	}

	public function getDisplayedColumns($categoryId){
		$category = Category::where('id','=',$categoryId)->firstOrFail();
		$allNonEmptyColumns = $this->removeEmptyColumns($categoryId);
		$displayed_columns = $category->displayed_fields;
		// $displayed_columns = $category->displayed_fields;
		if(trim($displayed_columns) == null || trim($displayed_columns) == ""){
			$displayed_columns = $allNonEmptyColumns;
			return $displayed_columns;
		} else {
			$displayed_columns = explode("-", substr($displayed_columns, 6));
		}

		//format columns
		$formattedDisplayColumns = array();//Why won't array_values work??? :'(
		for($i = 0;  $i < count($displayed_columns); $i++){
			$displayed_columns[$i] = trim($displayed_columns[$i]);
		}
		for($i = 0; $i < count($displayed_columns); $i++){
			if(in_array($displayed_columns[$i], $allNonEmptyColumns)){
				array_push($formattedDisplayColumns, $displayed_columns[$i]);
			}
		}
		return $formattedDisplayColumns;
	}

	public function removeEmptyColumns($category_id){
		$items = Item::where('category_id', '=', $category_id);
		$columns = $this->removeId(Schema::getColumnListing('items'));
		$columnsWithoutEmpty = array();
		for($i = 0; $i < count($columns); $i++){
			$count = DB::table('items')->where('category_id', '=', $category_id)->count($columns[$i]);

			if($count !== 0){
				array_push($columnsWithoutEmpty,$columns[$i]);
			}
		}
//		dd($columnsWithoutEmpty);
		 return $columnsWithoutEmpty;
	}
}
