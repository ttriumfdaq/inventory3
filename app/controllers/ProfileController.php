<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/1/15
 * Time: 11:08 AM
 */


class ProfileController extends BaseController {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */


    public function createNew(){

        if(!User::isValid(Input::all())){
            return Redirect::to('/root/createnew')->withErrors(User::$messages)->withInput();
        }
        else
        {
            $user = new User;
            $user->user_name = Input::get('user_name');
            $user->hashed_password = Hash::make(Input::get('hashed_password'));
            $user->save();

            if($user)
            {
                $addedUser = true;
                return View::make('root/after-user-is-added-view')->with('addedUser', $addedUser);
            }
            else
            {
                $addedUser = false;
                return View::make('root/after-user-is-added-view')->with('addedUser', $addedUser);
            }
        }
    }
    public function signIn()
    {
        //Posted from main.blade.php
        // How to implement http://laravel.com/docs/4.2/security
//        $auth = Auth::attempt(
//        array(
//            'user_name' => Input::get('username'), // Check email
//            'password' => Input::get('password')
//            )
//        );
//       echo Input::get('password');
        $username = Input::get('user_name');
        $password = Input::get('hashed_password');
        $hashed = Hash::make($password);

//        $user = User::where('user_name', '=', $username)->where('hashed_password', '=', $password)->first();
//        if($user){
//            Redirect::to('home');
//        } else {
//            echo "Login failed.";
//
//        }
        $credentials = array('user_name' => $username, 'hashed_password' => $password);
        if (Auth::attempt($credentials))
        {
            echo "You've logged in";
//            $username = Input::get('user_name');
//            $user = User::where('user_name', '=', $username);
//
//            //  return View::make('blog')->with('posts', $posts);
//            if ($user->count()){
//
//                $user = $user->first();
//
//               // return View::make('home')->with('user',$user);
//                echo "Logged in";
//            }
//            return "<p>Sorry, profile not found.</p>";
        }
        else
        {

            echo "Username: " . $username . "<br>";
            echo "Password: " . $password . "<br>";
            echo $hashed;
        }
    }

    public function editUser($id){
        echo $id;
    }

    public function deleteUser($id){
        echo $id;
    }

    public function changePassword(){
        return View::make('change_password');
    }

    public function postChangePassword(){

    }

}

