<?php

class HomeController extends BaseController {
//    protected $retiredSetting;
//
////
////    public function reset($categoryId){
////            if(Session::has('retired')){
////                Session::forget('retired');
////            }
////
////            if(Session::has('sesh' . $categoryId)){
////                Session::forget('sesh' . $categoryId);
////            }
////    }
//    protected $idArray = array("id", "category_id", "kind_id");
//
//    public function removeId($array){
//        //removes id, kind id and category id
//        for($i = 0; $i < count($array); $i++){
//            if( in_array($array[$i], $this->idArray)){
//                unset($array[$i]);
//            }
//        }
//        $new = array_values($array);
//
//        return $new;
//    }
//    public function getHiddenColumns($categoryId){
//
//        $displayed_columns = $this->getDisplayedColumns($categoryId);
//        $hidden_columns = array();
//
//
//
//        $all_columns = Schema::getColumnListing('items');
//        $all_columns = $this->removeId($all_columns);
//        for ($i=0; $i < count($all_columns); $i++) {
//            if(!in_array($all_columns[$i], $displayed_columns)){
//                array_push($hidden_columns, $all_columns[$i]);
//            }
//        }
//        return $hidden_columns;
//    }
//
//    public function getDisplayedColumns($categoryId){
//        $category = Category::where('id','=',$categoryId)->firstOrFail();
//        $displayed_columns = $category->displayed_fields;
//        // $displayed_columns = $category->displayed_fields;
//
//        //Turn string from database into array
//        $displayed_columns = explode("-", substr($displayed_columns, 6));
//
//        //format hidden columns
//        for($i = 0;  $i < count($displayed_columns); $i++){
//            $displayed_columns[$i] = trim($displayed_columns[$i]);
//        }
//        $this->removeId($displayed_columns);
//        return $displayed_columns;
//    }

    public function showRetiredItems(){
        $retired = Setting::where('var', '=', 'show_retired_items')->firstOrFail();
        if( is_null($retired)){
            $retired = new Setting;
            $retired->var = "show_retired_items";
            $retired->value = "false";
            $retired->save();
        }
        return $this->retiredSetting->value;
    }
    public function homePage(){
        $categories = Category::all();
        $numberOfUsers = User::count();
        $numberOfItems = Item::count();
        $numberOfCategories = Category::count();
        return View::make('home')->with(array(
            'categories' => $categories,
            'numberOfUsers' => $numberOfUsers,
            'numberOfItems' => $numberOfItems,
            'numberOfCategories' => $numberOfCategories
        ));
    }

    public function index()
    {
        try {
            $categoryHeadings = Category::where('parent_id', '=', 1)->get();

            //return $profile;
        }
        catch(Exception $e) {
            return $e->getMessage();
        }

        //return $categoryHeadings;
        return View::make("home.index")->with('categoryHeadings', $categoryHeadings);
    }

    public function search(){
        $searchTerm = Input::get('keyword');
        $counter = 1;
        $results = Kind::where('name', 'LIKE', "%$searchTerm%")
            ->get();
        $itemResults = Item::where('manufacturer', 'LIKE', "%$searchTerm%")->get();
        $arrayOfItemSearchResults = [];
        //echo $results[0]->name;

            foreach ($results as $result){
                $items = Item::where('kind_id', '=', $result->id)->get();
               // array_push($arrayOfItemSearchResults,$item);
               if ($items){
                   foreach ($items as $item){
                       array_push($arrayOfItemSearchResults, $item);
                   }
               }
            }

            return View::make('search-results')->with(array(
               'searchResults' => $arrayOfItemSearchResults,
                'counter' => $counter
            ));

    }

    public function lists($categories)
    {
        try {
            $categoryPath = array();
            $categoryItems = array();

            $categoryItem = Category::where('id', '=', $categories)->firstOrFail();  //Category by id

            array_push($categoryPath, $categoryItem);
            //$categoryPath = array_add($categoryPath, 0, $categoryItem);
            //$categoryItems = array_add($categoryItems, $categoryItem->id, $categoryItem);

            $categoryParent = Category::where('id', '=', $categoryItem->parent_id)->firstOrFail();
            //$pathCount = 1;
            while($categoryParent->id != 1) {
                array_push($categoryPath, $categoryParent);
                //$categoryPath = array_add($categoryPath, $pathCount, $categoryParent);
                //$pathCount+= 1;
                $categoryParent = Category::where('id', '=', $categoryParent->parent_id)->firstOrFail();

            }

            $categoryChild = Category::where('parent_id', '=', $categories)->get();

//            array_push($categoryPath, $categoryChild);
            $catChildCount = $categoryChild->count();


            if($catChildCount == 0) {
                //Session::forget('save_database');

                //Get retired setting
                if(Session::has('retired')){
                    if(Session::get('retired') == "true"){
                        $retiredSetting = "true";
                    } else {
                        $retiredSetting = "false";
                    }
                } else {
                    $retiredSetting = Setting::where('var', '=', 'show_retired_items')->firstOrFail()->value;
                }

                $items = Item::where('category_id', '=', $categoryItem->id)->get();


                if(isset($_GET["reset"]) && $_GET["reset"] === "true"){

                    if(Session::has('sesh' . $categories)){
                        Session::forget('sesh' . $categories);
                    }

                    if(Session::has('retired')){
                        Session::forget('retired');
                    }
                }
//
                $columns = $this->getDisplayedColumns($categories);
                $oneItem = Item::where('category_id', '=', $categoryItem->id)->first();
                if(! $oneItem){
                    return View::make('home.no_items');
                }
                $array = $columns;
                $newArray = $columns;
                $counter = 1;
                return View::make('root.final_level_category')->with(array(
                    'category' => $categoryItem,
                    'items' => $items,
                    'columns' => $columns,
                    'numberOfColumns' => count($columns),
                    'counter' => $counter,
                    'oneItem' => $oneItem,
                    'newArray' => $newArray,
                    'array' => $array,
                    'showRetiredItems' => $retiredSetting,
                    'categories' => $categories
                ));
            }
            else {
                foreach($categoryChild as $cChild){
                    array_push($categoryItems, $cChild);
                }
            }

        }
        catch(Exception $e) {
            return $e->getMessage();
        }
        $categoryPath = array_reverse($categoryPath);

        return View::make("home.lists")->with(array(
            'categoryPath' => $categoryPath,
            'categoryItems' => $categoryItems,
            'category' => $categoryItem
        ));
    }

    public function contactAdmin(){
        $inputAll = Input::all();


        $validator = Validator::make($inputAll, array('name' => 'required', 'comments' => 'required'));

        if ($validator->fails())
        {
            $errors = $validator->messages();

            return Redirect::back()->with('errors', $errors)->withInput();
        }
        $data = $inputAll;


        Mail::queue('emails.welcome', array('name' => Input::get('name'), 'comments' => Input::get('comments')), function($message){
            $message->to('catherineyangli@outlook.com')
                ->subject('Message from DAQ Inventory');
        });

        return Redirect::to('/home');
    }
    public function saveInDatabase($columns, $categoryItem){
        //Saves settings in database rather than sessions
        $stringOfColumns = "--- \n- " . implode("\n- ",($columns ));
        $categoryItem->displayed_fields = $stringOfColumns;

        //Save hidden columns
        $hidden_columns = $this->getHiddenColumns($categoryItem->id);
        $hidden_columns_final = array();
//        for($i = 0; $i < count($hidden_columns); $i++){
//            if(!in_array($hidden_columns[$i], $columns)){
//                array_push($hidden_columns[$i], $hidden_columns_final);
//            }
//        }
//
//        $categoryItem->hidden_fields = $hidden_columns_final;
        $categoryItem->save();
        $hidden_columns = $this->getHiddenColumns($categoryItem->id);
        $hidden_columns = "--- \n- " . implode("\n- ",($hidden_columns));
        $categoryItem->hidden_fields = $hidden_columns;
        $categoryItem->save();
    }

    public function saveInSessions(){
        //Saves settings in sessions storage
    }
    public function selectCategory($categories){

        $categoryItem = Category::where('id', '=', $categories)->firstOrFail();//Get category
        $items = Item::where('category_id', '=', $categoryItem->id)->get();//get items under category

        $columns = array();
//
        $numberOfSelectedCategories = Input::get('numberOfSelectedCategories');
        for($i = 0; $i < $numberOfSelectedCategories; $i++){
            $cat = Input::only('category_selection' . $i);
            $value = $cat['category_selection' . $i];
            array_push($columns, $value);
        }


        if (isset($_POST['action']) && $_POST['action'] == 'save_database') {

            $retiredSetting = Setting::where('var', '=', 'show_retired_items')->firstOrFail();
            //action for update here
            $this->saveInDatabase($columns, $categoryItem);
            if (isset($_POST['retired'])) {
                $retiredSetting->value = "true";
                $retiredSetting->save();
                Session::put('retired', 'true');
                $showRetired = "true";
            } else {
                $retiredSetting->value = "false";
                $retiredSetting->save();
                Session::put('retired', 'false');
                $showRetired = "false";
            }
        } else {
            if(isset($_POST['retired'])){
                Session::put('retired', 'true');
                $showRetired = "true";
            } else {
                Session::put('retired', 'false');
                $showRetired = "false";
            }
        }
            //actions for session
        Session::put('sesh' . $categories, $columns);

         //The first argument is the key, the second is the value
        $numberOfColumns = count($columns);

        $oneItem = Item::where('category_id', '=', $categoryItem->id)->first();
        if(! $oneItem){
            return ('No item in this category. Click here to add one.');
        }
        $array = $columns;//Fix
        $newArray = $columns;//must fix!!!
        $counter = 1;

        return View::make('root.final_level_category')->with(array(
            'category' => $categoryItem,
            'items' => $items,
            'columns' => $columns,
            'numberOfColumns' => $numberOfColumns,
            'counter' => $counter,
            'oneItem' => $oneItem,
            'newArray' => $newArray,
            'array' => $array,
            'showRetiredItems' => $showRetired,
            'categories' => $categories,

        ));
    }


}
