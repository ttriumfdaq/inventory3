<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/2/15
 * Time: 1:33 PM
 */

class SessionsController extends BaseController {

    public function index(){
        return "";
    }
    public function create (){
        return View::make('sessions.create');
    }

    public function store() {
        //$sha1Password = sha1(Input::get('hashed_password'));
        if(Auth::attempt(Input::only('hashed_password', 'user_name'), false)) {
            return Redirect::back()->with(array('user' => Auth::user(), 'success-message' => 'You are now signed in')) ;
        } else {
            return Redirect::back()->with(array('error-message' => 'Invalid username/password combination'));
        }
    }

    public function destroy(){
        Auth::logout();
        return Redirect::back()->with(array('logout-message' => 'You have been logged out.'));
    }
}