<?php
class ConfigurationController extends BaseController {

    public function viewAllCategories(){
        $categories = Category::all();
        $categoryHeadings = Category::where('parent_id', '=', 1)->get();
        return View::make('root.view-all-categories')
            ->with(array('categories' => $categories, 'categoryHeadings' => $categoryHeadings));
    }

    public function changeCategory($id){
        echo $id;
    }

    public function index(){
        $inputFields = Input::all();
        $users = User::all();
        $categoryHeadings = Category::where('parent_id', '=', 1)->get();
        return View::make('root.configuration-interface')->with(array(
            'inputFields' => $inputFields,
            'users' => $users,
            'categoryHeadings' => $categoryHeadings
        ));
    }

    public function addNewCategory(){

        $parentCategoryID = Input::get('category_parent');
        $parentCategory = Category::where('id', '=', $parentCategoryID)->first();


      //  $parentCategoryID = $parentCategory->id;
        $newlyAddedCategory = new Category;
        $newlyAddedCategory->parent_id = $parentCategoryID;
        $newlyAddedCategory->name = Input::get('category_name');
        $newlyAddedCategory->description = Input::get('category-description');
        $newlyAddedCategory->save();
        $message = "";
        if($newlyAddedCategory){
            $message = '{{$newlyAddedCategory->name}} has been added. ';
            return View::make('root/view-all-categories')->with('message', $message);

        } else {
            $message = 'Category has not been added. Please try again.';
            return View::make('root/view-all-categories')->with('message', $message);

        }
    }
}