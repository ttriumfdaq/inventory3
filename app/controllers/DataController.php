<?php

class DataController extends BaseController {


    public function getSelectColumn($category_id){

        $hidden_columns = array();
        $all_categories = $this->removeEmptyColumns($category_id);

        if(Session::has('sesh' . $category_id)){
            $displayed_columns = Session::get('sesh' . $category_id);
            for($i = 0; $i < count($all_categories); $i++){
                if(! in_array($all_categories[$i], $displayed_columns)){
                    array_push($hidden_columns, $all_categories[$i]);
                }
            }
        } else {
            $hidden_columns = $this->getHiddenColumns($category_id);
            $displayed_columns = $this->getDisplayedColumns($category_id);
        }

        if(Session::has('retired')){
            if(Session::get('retired') == "true"){
                $retiredSetting = "true";
            } else {
                $retiredSetting = "false";
            }
        } else {
            $retiredSetting = Setting::where('var', '=', 'show_retired_items')->firstOrFail()->value;
        }
        return View::make('partials.select-col')->with(array(
            'hidden_columns' => $hidden_columns,
            'displayed_columns' => $displayed_columns,
            'retired' => $retiredSetting,
            'categories' => $category_id));
    }

    public function showTable($categories){
        $categoryItem = Category::where('id', '=', $categories)->firstOrFail();
        $retiredSetting = Setting::where('var', '=', 'show_retired_items')->firstOrFail();
        $items = Item::where('category_id', '=', $categoryItem->id)->get();
        if(!Session::has('retired')){
            Session::put('retired', "false");
        }

        if(isset($_GET["reset"]) && $_GET["reset"] === "true"){
            if(Session::has('retired')){
                Session::forget('retired');
            }

            if(Session::has('sesh' . $categories)){
                Session::forget('sesh' . $categories);
            }
        }

        $columns = $this->getDisplayedColumns($categories);
        $oneItem = Item::where('category_id', '=', $categoryItem->id)->first();
        if(! $oneItem){
            return ('No item in this category. Click here to add one.');
        }
        $array = $columns;
        $newArray = $columns;
        $counter = 1;
        return View::make('partials.datatable')->with(array(
            'category' => $categoryItem,
            'items' => $items,
            'columns' => $columns,
            'numberOfColumns' => count($columns),
            'counter' => $counter,
            'oneItem' => $oneItem,
            'newArray' => $newArray,
            'array' => $array,
            'showRetiredItems' => $retiredSetting,
            'categories' => $categories
        ));
    }

    public function postEditable(){
        $array = explode("|", $_REQUEST['id']);

        $item_id = $array[0];
        $column = $array[1];

        $item = Item::where('id', '=', $item_id)->firstOrFail();

        $user_id = 0;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }
        $created_on = date('Y-m-d G:i:s');
        $old_value = trim($item->$column);
        $new_value = trim($_REQUEST['value']);
        if($old_value !== $new_value){
            $item->updated_on = $created_on;
            $item->$column = $new_value;
            $item->save();
        }
        if($item->save() && $old_value !== $new_value){
            $new_change_log = new Change;
            $new_change_log->item_id = $item_id;
            $new_change_log->user_id = $user_id;
            $new_change_log->field = $column;
            $new_change_log->old_value = $old_value;
            $new_change_log->new_value = $new_value;
            $new_change_log->created_on = $created_on;

            $new_change_log->save();
            echo $_POST['value'];
        } else {
            echo $old_value;
        }
    }

}