<?php namespace Illuminate\Hashing;

use Illuminate\Support\ServiceProvider;

#use App\ShaHasher;
#use Illuminate\Hashing\HashServiceProvider;

class SHAHashServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app['hash'] = $this->app->share(function () {
            return new SHAHasher();
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array('hash');
    }

}
