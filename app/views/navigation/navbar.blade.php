<?php

try {
    $categoryHeadings = Category::where('parent_id', '=', 1)->orderBy('name')->get();
}
catch(Exception $e) {
    return $e->getMessage();
}


?>

<div class="row">
<!-- uncomment code for absolute positioning tweek see top comment in css -->
<!--<div class="absolute-wrapper"> </div>-->
<!-- Menu -->
<div class="side-menu">

<nav class="navbar navbar-default" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
    <div class="brand-wrapper">
        <!-- Hamburger -->
        <button type="button" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>

        <!-- Brand -->
        {{--<div class="brand-name-wrapper">--}}
            {{--<a class="navbar-brand" href="#">--}}

            {{--</a>--}}
        {{--</div>--}}

        <!-- Search -->
        {{--<a data-toggle="collapse" href="#search" class="btn btn-default" id="search-trigger">--}}
            {{--<span class="glyphicon glyphicon-search"></span>--}}
        {{--</a>--}}

        <!-- Search body -->
        {{--<div id="search" class="panel-collapse collapse">--}}
            {{--<div class="panel-body">--}}
                {{--<form class="navbar-form" action="#" method="post" role="search">--}}
                    {{--<div class="form-group">--}}
                        {{--<input type="text" class="form-control" name="keyword" placeholder="Search">--}}
                    {{--</div>--}}
                    {{--<button type="submit" class="btn btn-default "><span class="glyphicon glyphicon-ok"></span></button>--}}
                {{--</form>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

</div>

<!-- Main Menu -->
<div class="side-menu-container">
<ul class="nav navbar-nav">
    <li class="active"><a href={{URL::to('/home')}}><span class="glyphicon glyphicon-home"></span> Home</a></li>
    <li class="active"><a href="https://ladd00.triumf.ca/daqinv/frontend"><span class="glyphicon glyphicon-home"></span> Old Table Editor</a></li>

@if(Auth::check())
@if(Auth::user()->role == 'admin')
    <li style="background-color: #2e6da4" class="panel panel-default configuration" id="dropdown">
        <a data-toggle="collapse" href="#dropdown-lvl1-100">
            <span class="fa fa-desktop"></span> Configuration <span class="caret"></span>
        </a>
        <div id="dropdown-lvl1-100" class="panel-collapse collapse">
            <div class="panel-body">
                <ul class="nav navbar-nav">
                    <li><a href="{{URL::to('root/view-all-users')}}">Users</a></li>
                    <li><a href="{{URL::to('root/view-all-categories')}}">Categories</a></li>
                </ul>
            </div>
        </div>
    </li>
@endif
@endif


<!-- Dropdown-->




<?php $counter = 1; ?>
<!-- computing -->
@foreach($categoryHeadings as $categoryHeading)
<li class="panel panel-default" id="dropdown">
    <a data-toggle="collapse" href="#dropdown-lvl1-{{$counter}}">
        <span class="fa fa-desktop"></span> {{$categoryHeading->name}} <span class="caret"></span>
    </a>

    <!-- Dropdown level 1 -->
<!--    <div id="dropdown-lvl1-2" class="panel-collapse collapse">-->
    <div id="dropdown-lvl1-{{$counter}}" class="panel-collapse collapse">
        <div class="panel-body">
            <ul class="nav navbar-nav">
                <?php
                $levelTwoCategories = Category::where('parent_id', '=', $categoryHeading->id)->orderBy('name')->get();
                ?>
                @if(!empty($levelTwoCategories))
                    @foreach($levelTwoCategories as $levelTwoCategory)
                        <li><a href="{{URL::route('get-child-category', $levelTwoCategory->id)}}">{{$levelTwoCategory->name}}</a></li>
                    @endforeach

                @endif
            </ul>
        </div>
    </div>
</li>
<?php $counter++; ?>
@endforeach


</ul>
</div><!-- /.navbar-collapse -->
</nav>

</div>




