@extends('layouts.main')
@section('maincontent')
<?php
$categories = Category::all();
$numberOfUsers = User::count();
$numberOfItems = Item::count();
$numberOfCategories = Category::count();

?>
<div class="page-header">
<h2>Welcome to the SciTech Dept inventory system</h2>
</div>
  <p>  This system keeps track of TRIUMF Data Acquisition Group and Electronics Lab equipment.

    To learn more about the system, consult the <a href="#">help documentation</a>.

    If you need a username and password, please contact
    <a href="#">Konstantin Olchanski</a>.
    </p>

<pre>
     System statistics

Files: 0 total, 0 images, 0 documentation files
Items: {{$numberOfItems}} total, 112 retired, 2181 non-retired
Categories: {{$numberOfCategories}}
Users: {{$numberOfUsers}}
</pre>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Table Functions</h3>
  </div>
  <div class="panel-body">
    <p>
    1.	Sorting <br>
    -	Click on column header to sort by particular column (example: Type) <br>
    -	Sorting will toggle between ascending and descending<br>
    -	Multi-column sorting: Shift + click on a second column header to sort by second column
    </p>
    <p>
    2.	Inline Editing<br>
    -	Click cell to edit value<br>
    -	Press Enter while inside the cell to save the value<br>
    -	Press Escape while inside the cell to cancel the edit
    </p>
    <p>
    3.	Searching<br>
    -	Filters items by type name in real time
    </p>
    <p>
    4. (Button)	Click on Options to configure display settings
    </p>
    <p>Options</p>
    <ul>
      <li>Drag and drop column names into "Displayed Columns" or "Hidden Columns" to configure table display</li>
      <li>Select "Show Retired Items" if you would like to include items whose status is "retired"</li>
      <li> (Button) "Save for Session" will temporarily save your settings for the duration of your session.</li>
      <li>(Button) "Save for All Users" will persist settings in the database</li>
      <li>(Button) "Cancel" will hide the options menu</li>
      <li>(Button) "Restore Settings from DB will erase your session options.</li>
    </ul>
    <p>
      5. (Button) Click on "Go to Old Table" to go to original inventory system
    </p>
  </div>
</div>

@stop
