@extends('layouts.main')
@section('maincontent')

<h1>{{$categoryPath[1]->name}}</h1>


@foreach($categoryItems as $catItems)

    <?php $subCategories1 = Category::where('parent_id', '=', $catItems->id)->get();
        $itemsCount = Item::where('category_id', '=', $catItems->id)->get()->count();
    ?>
    @if(count($subCategories1) === 0)
        <li>{{$categoryPath[1]->name}} >>
        <a href="{{URL::to('daqinv/lists/' . $catItems->id)}}">{{$catItems->name}}({{$itemsCount}})</a>
        </li>
    @else
        @foreach($subCategories1 as $sub1)
            <?php
                $itemsCount2 = Item::where('category_id', '=', $sub1->id)->get()->count();
            ?>
            <li>{{$categoryPath[1]->name}} >> {{$catItems->name}} >>
                <a href="{{URL::to('daqinv/lists/' . $sub1->id)}}">{{$sub1->name}}({{$itemsCount2}})</a>
            </li>
        @endforeach
    @endif
@endforeach





@stop
