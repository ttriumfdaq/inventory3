<?php
$categories = Category::all();
$categoryNames = "";
    foreach($categories as $category)  {
        $categoryNames .= $category->name . ", ";
    }

?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    {{ HTML::style('awesomplete/awesomplete.css') }}
</head>
<body>
<input class="awesomplete"
       data-list="{{$categoryNames}}" />

{{ HTML::script('awesomplete/awesomplete.js') }}
</body>
</html>


