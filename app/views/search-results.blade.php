@extends('layouts.main')
@section('maincontent')
<?php

?>
<input type="button" value="Reset" onClick="window.location.reload()">
<button>Show All Fields</button>
<br>
<table data-toggle="table" id="mytable" class="table-bordered table table-hover">
    <thead>
    <tr><th>Type</th>

        <th>Manufacturer <br><input id="chk1" type="checkbox" checked></th>
        <th>Model <br><input id="chk2" type="checkbox" checked></th>
        <th>Description <br><input id="chk3" type="checkbox" checked></th>
        <th>Barcode <br><input id="chk4" type="checkbox" checked></th>
        <th>Quantity <br><input id="chk5" type="checkbox" checked></th>
    </tr>
    </thead>



    @foreach($searchResults as $searchResult) <!-- one item = one row -->
    <tr>
        <td> <?php
            $kind = Kind::where('id', '=', $searchResult->kind_id)->firstOrFail();
            if($kind){
                echo $kind->name;
            }
            ?> </td>

        <td>{{$searchResult->manufacturer}}</td>
        <td>{{$searchResult->model}}</td>
        <td>{{$searchResult->description_too}}</td>
        <td>{{$searchResult->barcode}}</td>
        <td>{{$searchResult->quantity}}</td>
    </tr>
    @endforeach

</table>
<?php //echo $items->links(); ?>
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script>
    $("#mytable th").each(function(i) {
        var remove = 0;

        var tds = $(this).parents('table').find('tr td:nth-child(' + (i + 1) + ')')
        tds.each(function(j) { if (this.innerHTML == '') remove++; });

        if (remove == ($('#mytable tr').length - 1)) {
            $(this).hide();
            tds.hide();
        }
    });

    $(function() {
        $("#mytable input[type=checkbox]").on("change", function(e) {
            var id = $(this).parent().index()+1,
                col = $("table tr th:nth-child("+id+"), table tr td:nth-child("+id+")");
            $(this).is(":checked") ? col.show() : col.hide();
        });

        $("button").on("click", function(e) {
            $("input[type=checkbox]").prop("checked", true).change();
        });
    })

</script>
@stop





