@extends('layouts.main')
@section('maincontent')

    {{Form::open(array('route' => 'contact-admin-post', 'class' => 'form-vertical col-lg-6'))}}
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Name</label>
        <input type="text" value="{{ Input::old('name'); }}" class="form-control" name="name" id="name">
    </div>

    @if ($errors->has('name'))
        <div class="alert alert-danger">
        {{ $errors->first('name') }}
        </div>
    @endif

    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Comments</label>
        <textarea class="form-control"  name="comments" id="comments">{{ Input::old('comments'); }}</textarea>
    </div>

    @if($errors->has('comments'))
        <div class="alert alert-danger">
        {{ $errors->first('comments') }}
        </div>
        @endif
    <br>
   <button type="submit" class="btn btn-default">Send</button>
    {{Form::close()}}
@stop