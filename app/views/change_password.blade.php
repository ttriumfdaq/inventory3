<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/1/15
 * Time: 10:36 AM
 */

?>
@extends('layouts.main')
@section('maincontent')

{{Form::open(array('route' => 'post-change-password'))}}
    Old password: <input name="old_password" type="password">
    New password: <input name="new_password" type="password">
    Confirm new password: <input name="confirm_new_password" type="password">
    <input type="submit" value="Change Password">
{{Form::close()}}

@stop