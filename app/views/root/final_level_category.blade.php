<?php
include(app_path().'/includes/table.php');
define("LINK_TO_OLD_TABLE", "https://ladd00.triumf.ca/daqinv/frontend/list/" . $categories);
define("LINK_TO_ITEM", "https://ladd00.triumf.ca/daqinv/frontend/show/");
$retiredItemsCount = 0;
?>
@extends('layouts.main')
@section('maincontent')
    @if($showRetiredItems == "false")
        <script>
            $(document).ready(function(){
                $('.retired').addClass("importantRule");
            });
        </script>
    @endif
<?php


?>

    <button style="display: inline-block" id="old_table">Go to Old Table</button>
    <button id="display_options">Options</button><h3 style="display: inline-block">&nbsp;&nbsp;{{$category->name}} ({{count($items) }} items in this category)</h3>
    <div id="wrapper"><div id="options"></div></div>

    <table data-toggle="table" id="mytable" class="tablesorter table-bordered table table-hover">

        <thead>
        <tr>
            <th></th>
            <th>Type</th>
            @foreach($columns as $column)
                <th id={{$column}}>{{ucfirst(str_replace("_", " ", $column))}}<br></th>
                <?php $counter++;?>
            @endforeach
        </tr>
        </thead>


        <tbody>
        @foreach($items as $item) <!-- one item = one row -->

        @if($item->status == "retired")
            <tr class="retired" id="retired"><?php $retiredItemsCount++; ?>
        @else
            <tr>
        @endif
                <td><input id="{{$item->id}}" type="checkbox"></td>
                <td id="test"> <?php
                    $kind = Kind::find($item->kind_id);
                    if($kind){ ?>
                    <a href="{{LINK_TO_ITEM . $item->id}}"> {{ $kind->name }}   </a>
                    <?php    } ?>
                </td>
                @for($i = 0; $i < $numberOfColumns; $i++)
                    @if($newArray[$i] == "created_on" ||$newArray[$i] == "updated_on"|| !Auth::check())
                        <td class="uneditable" id="{{$item->id}}|{{$newArray[$i]}}">@else<td class="editable_cell" id="{{$item->id}}|{{$newArray[$i]}}">@endif<?php  if( $item[$newArray[$i]] != "" && $item[$newArray[$i]] != null ){ ?>{{trim($item[$newArray[$i]])}}<?php } ?></td>
		@endfor
            </tr>
        @endforeach
        </tbody>
    </table>
    <p>Retired items: {{ $retiredItemsCount }}</p>
    <!--
<div id="group" style="display:none">
    <form id="group_options" role="form">
        <div class="form-group">
            <label for="usr">Name:</label>
            <input type="text" class="form-control" id="usr">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd">
        </div>
    </form>
</div>-->
<script>
    $(document).ready(function(){
        $('#mytable').dataTable({
            "order": [[ 1, "asc" ]],
            paging: false,
            columnDefs: [
                { type: 'natural'}
            ]
        });
        $('#mytable td[class="editable_cell"]').editable("{{URL::route('testEditable')}}",
                {onblur: "ignore", onkeypress: "submit"}
        );
        $('#options').load("{{URL::to('/select-col', $category->id)}}").hide();

        $('#display_options').click(function(){
            $('#options').toggle(200);
        });
    });
</script>
<script>
    $(document).ready(function(){
        $('#mytable th').each(function(i) {
            var remove = 0;
            var string = "";
            var tds = $(this).parents('table').find('tr td:nth-child(' + (i + 1) + ')');
            tds.each(function(j) { if (this.innerHTML.trim() == '') remove++; });
            var tableHeader = $('#mytable th');
            if (remove == ($('#mytable tr').length - 1)) {
                string += $(this);
                $(this).hide();
                tds.hide();
            }

        });
        $('.uneditable').dblclick(function(){
            alert("Cannot inline edit. Please ensure you are signed in and you are editing the correct column.");
        });
        $('#disabled_button').click(function(){
            alert("Please log in.")
        });

        $('#old_table').click(function(){
            location.href = "{{ LINK_TO_OLD_TABLE }}";
        });
    });
</script>

@stop






