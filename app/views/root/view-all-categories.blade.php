@extends('layouts.main')
@section('maincontent')
<?php
    $inputFields = Input::all();
    if(isset($message)){
        echo $message;
    }
$categoryNames = "";
foreach($categories1 as $category1)  {
    $categoryNames .= $category1->name . ", ";
}

echo $categoryNames;
?>

<form id="editCategories" method="post" action="">
    <ul>
        @foreach($categories1 as $category1)
        <li>
            <a href="{{URL::to('root/change_category_name', $category1->id)}}">{{$category1->name}}</a>
            <?php
            $subcategories = Category::where('parent_id', '=', $category1->id)->get();
            if(! empty($subcategories)){
                echo '<ul>';
                foreach($subcategories as $subcategory) { ?>
                     <li><a href="{{URL::to('root/change_category_name', $subcategory->id)}}">  {{$subcategory->name}}</a></li>
        <?php
                    $thirdLevelCategories = Category::where('parent_id', '=', $subcategory->id)->get();
                    if(! empty($thirdLevelCategories)){
                        echo '<ul>';
                        foreach ($thirdLevelCategories as $thirdLevelCategory){ ?>
                            <li><a href="{{URL::to('root/change_category_name', $thirdLevelCategory->id)}}">{{$thirdLevelCategory->name }}</a></li>
        <?php
                        }
                        echo '</ul>';
                    }
                    echo '</li>';
                }
                echo '</ul><br>';
            }
            ?>
            @endforeach
        </li>
    </ul>

    <input type="submit" value="Save All Changes">
</form>


    <h2>Add A New Category</h2>
    <form class="form-horizontal" role="form" method="post" action="#">
        <div class="form-group">
            <label class="control-label col-sm-2" for="category_name">Name of Category: </label>
            <div class="col-sm-5">
                <input type="text" data-list="{{$categoryNames}}"  class="awesomplete form-control" name="category_name" required>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="category-description">Description: </label>
            <div class="col-sm-5">
                <input type="text" class="form-control" name="category-description" >
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="category_parent">Parent Category: </label>
            <div class="col-sm-10">
                <select name="category_parent" id="category_parent" required="required" >
                    <?php
                    foreach ($categories1 as $category1){
                        echo '<option value="' . $category1->id . '">' . $category1->name . '</option>';
                        $categories2 = Category::where('parent_id', '=', $category1->id)->get();
                        if( ! $categories2){
                            echo '<option value="' . $category1->id . '">' .$category1->name . '</option>';
                        } else {
                            foreach ($categories2 as $category2){
                                echo '<option value="' . $category2->id . '">' .$category1->name . '>>' . $category2->name . '</option>';
                                $categories3 = Category::where('parent_id', '=', $category2->id)->get();
                                if (! $categories3){
                                    echo  '<option value="' . $category2->id . '">' . $category1->name . " >>" . $category2->name . '</option>';
                                } else {
                                    foreach ($categories3 as $category3) {
                                        echo '<option value="' . $category3->id . '">' . $category1->name . '>>' . $category2->name . '>>' . $category3->name . '</option>';
                                        $categories4 = Category::where('parent_id', '=', $category3->id)->get();
                                        if(! $categories4){
                                            echo '<option value="' . $category3->id . '">' . $category1->name . ">>" . $category2->name . '>>' . $category3->name . '</option>';
                                        } else {
                                            foreach ($categories4 as $category4){
                                                echo '<option value="' . $category4->id . '">'. $category1->name . ">>" . $category2->name . '>>' . $category3->name . ">>" . $category4->name . '</option>';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
{{ HTML::script('awesomplete/awesomplete.js') }}

@stop

