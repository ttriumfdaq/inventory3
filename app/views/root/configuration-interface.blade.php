@extends('layouts.main')

@section('maincontent')

<h2>What would you like to do? </h2>
<ul>
    <li>Edit Users</li>
    <li>Edit Categories</li>
</ul>

<table id="editUsers">
    @foreach($users as $user)
    <tr>

        <td>{{$user->user_name}}</td>

        <td><a href="{{URL::route('edit-user', $user->id)}}">Edit</a></td>

        <td><a href="{{URL::route('delete-user', $user->id)}}">Delete</a></td>
    </tr>
    @endforeach
</table>

<a href="{{URL::route('get-create-new-user')}}">Create a new user</a>



<form id="editCategories" method="post" action="">
    <ul>
        @foreach($categoryHeadings as $categoryHeading)
        <li><input type="text" name="{{$categoryHeading->id}}" value="{{$categoryHeading->name}}">
            <?php
            $subcategories = Category::where('parent_id', '=', $categoryHeading->id)->get();
            if(! empty($subcategories)){
                echo '<ul>';
                foreach($subcategories as $subcategory) {
                    echo '<li><input type="text" value="' . $subcategory->name . '">';
                    $thirdLevelCategories = Category::where('parent_id', '=', $subcategory->id)->get();
                    if(! empty($thirdLevelCategories)){
                        echo '<ul>';
                        foreach ($thirdLevelCategories as $thirdLevelCategory){
                            echo '<li><input type="text" value="' . $thirdLevelCategory->name . '">' .
                                '</li>';
                        }
                        echo '</ul>';
                    }
                    echo '</li>';
                }
                echo '</ul><br>';
            }
            ?>
            @endforeach

        </li>
    </ul>

    <input type="submit" value="Save All Changes">
</form>

<div class="container">
    <h2>Add A New Category</h2>
    <form class="form-horizontal" role="form" method="post" action="#">
        <div class="form-group">
            <label class="control-label col-sm-2" for="category-name">Name of Category: </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="category-name" >
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="category-parent">Parent: </label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="category-parent">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>
@stop