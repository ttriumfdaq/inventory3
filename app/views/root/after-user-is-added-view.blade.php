<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/3/15
 * Time: 8:11 PM
 */
?>

@extends('layouts.main')

@section('maincontent')

@if($addedUser == 'true')
    <p>User was added successfully! </p>
    <a href="{{URL::route('get-create-new-user')}}">Add Another User</a>
    @else
    <p>User was not added.  </p>
    <a href="{{URL::route('get-create-new-user')}}">Please try again.</a>

@endif
@stop