@extends('layouts.main')

@section('maincontent')
<?php
    $users = User::all();
?>
<table>
    @foreach($users as $user)
    <tr>

        <td>{{$user->user_name}}</td>

        <td><a href="{{URL::route('edit-user', $user->id)}}">Edit</a></td>

        <td><a href="{{URL::route('delete-user', $user->id)}}">Delete</a></td>
    </tr>
    @endforeach
</table>

<a href="{{URL::route('get-create-new-user')}}">Create a new user</a>


@stop