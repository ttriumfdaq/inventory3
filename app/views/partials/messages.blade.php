
    @if(Session::has('success-message'))
        <div class="alert alert-success">
            {{ Session::get('success-message') }}
        </div>
    @elseif(Session::has('error-message'))
        <div class="alert alert-danger"> {{ Session::get('error-message') }}
        </div>
    @elseif(Session::has('logout-message'))
        <div class="alert alert-info"> {{ Session::get('logout-message') }}
        </div>
    @endif


