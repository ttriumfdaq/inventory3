<?php

?>
<table data-toggle="table" id="mytable" class="tablesorter table-bordered table table-hover">

<thead>
<tr>
<th></th>
<th>Type</th>
@foreach($columns as $column)
    <th id={{$column}}>{{$column}} <br></th>
    <?php $counter++;?>
    @endforeach
    </tr>
    </thead>


    <tbody>
    @foreach($items as $item) <!-- one item = one row -->

    @if($item->status == "retired")
        <tr class="retired" id="retired">
    @else
        <tr>
            @endif
            <td><input id="{{$item->id}}" type="checkbox"></td>
            <td id="test"> <?php
                $kind = Kind::find($item->kind_id);
                if($kind){ ?>
                <a href="{{URL::route('get-item-details', $item->id)}}"> {{ $kind->name }}   </a>
                <?php    } ?>
            </td>
            @for($i = 0; $i < $numberOfColumns; $i++)
                @if($newArray[$i] == "created_on" ||$newArray[$i] == "updated_on"|| !Auth::check())
                    <td id="{{$item->id}}|{{$newArray[$i]}}">
                @else
                    <td class="editable_cell" id="{{$item->id}}|{{$newArray[$i]}}">
                        @endif
                        <?php  if( $item[$newArray[$i]] != "" && $item[$newArray[$i]] != null )
                        { ?> {{$item[$newArray[$i]]}}  </td> <?php } ?>
                    @endfor
        </tr>
        @endforeach
    </tbody>
    </table>