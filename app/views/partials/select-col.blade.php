
<div class="alert-dismissable">

<section id="connected">
    <ul id="sortable1" class="boo display connected list connectedSortable">
        <p>Displayed Columns</p>
        @foreach($displayed_columns as $dcolumn)
        <li class="ui-state-default" >{{$dcolumn}}</li>
        @endforeach
    </ul>

	<?php sort($hidden_columns); ?>

    <ul id="sortable2" class="connected list no2 connectedSortable">
        <p>Hidden Columns</p>
        @for($i = 0; $i < count($hidden_columns)/2; $i ++)
            <li class="ui-state-highlight">{{$hidden_columns[$i]}}</li>
        @endfor

    </ul>

    <ul id="sortable3" class="connectedSortable connected list no2">
        <p>Hidden Columns</p>
        @if(count($hidden_columns) % 2 === 0)
            @for($i = (count($hidden_columns)/2);  $i < count($hidden_columns); $i ++)
                <li class="ui-state-highlight">{{$hidden_columns[$i]}}</li>
            @endfor
        @else
            @for($i = (count($hidden_columns)/2) + 1;  $i < count($hidden_columns); $i ++)
                <li class="ui-state-highlight">{{$hidden_columns[$i]}}</li>
            @endfor
        @endif
    </ul>


</section>
{{ Form::open(array(
'method' => 'POST',
'id' => 'options_form',
'class' => 'selectedCategory',
'route' => array('selectCategory', $categories))
) }}

    <label for="retired"> Show retired items: </label>
    @if($retired === "true")
        <input type="checkbox" name="retired" checked>
    @else
        <input type="checkbox" name="retired">
    @endif


    <button id="add" type="submit">Save for Session</button>
    {{--<input value="Save for Session" name="action" id="add" type="submit">--}}
    @if(Auth::check())
        <input type="submit" value="Save for All Users" name="action" id="save_database" >
    @else
        <button id="disabled_button"  type="submit" value="Save for All Users" name="action" disabled>
            Save for All Users
        </button>
    @endif
    <button id="close"> Cancel </button>
    {{ Form::token() }}
{{Form::close()}}

        <button id="reset">Restore Settings from DB</button>

</div>
{{ HTML::script('js/jquery.sortable.js') }}
<script>
    $(function() {
        $( "#sortable1, #sortable2, #sortable3" ).sortable({
            connectWith: ".connectedSortable"
        });
        $('#close').click(function(evt){
            evt.preventDefault();
            $('#options').hide(200);
        });
        $('#add, #save_database').click(function(){
            var counter = 0;
            var array = [];
            $('ul.display li').each(function(i, elem){
//                alert($(elem).id() + " " + $(elem).text());
                array.push($(elem).text());
                $('<input type="hidden" name="category_selection' + counter + '" value="' + $(elem).text() + '" />')
                        .appendTo($('.selectedCategory'));
                counter++;
            });

            //Get the number of selected categories.
            $('<input type="hidden" name="numberOfSelectedCategories' + '" value="' + counter + '" />')
                    .appendTo($('.selectedCategory'));

        });

    });
</script>

<script>
    $(function(){
            $('#save_database').click(function(){
                $('<input type="hidden" name="action' + '" value="save_database" />')
                        .appendTo($('.selectedCategory'));
            });
            $('#options_form').submit(function(evt){

                evt.preventDefault();
                //prevents default action from happening
                var url = $(this).attr("action");
                var formData = $(this).serialize();
//                if(buttonId === "save_database"){
//                    formData += "&save_database=true";
//                }
                $.ajax(url, {
                    data:formData,
                    type: "POST",
                    success : function(response){
                        $('#options_form').html(response);
                        //$('#options').hide(200);
                        location.href = location.href.replace("?reset=true", "");
                    }
                });
            });
        $('#reset').click(function(){
            location.href = "{{URL::route('get-child-category', array('category_id' => $categories, 'reset' => 'true'))}}";
        });

    });
</script>