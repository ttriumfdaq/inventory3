<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/2/15
 * Time: 1:39 PM
 */
?>
@extends('layouts.main')

@section('maincontent')
{{Form::open(['route' => 'sessions.store'])}}
<div class="form-group">
    {{Form::label('user_name', 'Username: ')}}
    {{Form::text('user_name', null, array('class' => 'form-control'))}}
</div>

<div>
    {{Form::label('hashed_password', 'Password: ')}}
    {{Form::password('hashed_password', array('class' => 'form-control'))}}
</div>
{{Form::token()}}
<br>
<div>
    {{Form::submit('Login', array('class' => 'btn btn-default'))}}
</div>
{{Form::close()}}

@stop