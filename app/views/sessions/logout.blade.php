<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/1/15
 * Time: 11:17 AM
 */

?>

@extends('layouts.main')

@section('maincontent')
<pre>You've been logged out.
<a href="{{URL::to('/login')}}">Log back in</a>
</pre>

@stop