@extends('layouts.main')

@section('maincontent')
    <button value="loadAjax">Load Ajax</button>
    <div id="loadAjax">
        Change me
    </div>

    <script>
        $(function(){
            $.get("ajax2",
                    function(data){
                        $("#loadAjax").html(data);
                    });

        });
    </script>
@stop