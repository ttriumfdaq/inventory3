@extends('layouts.main')
@section('maincontent')
<form method="post" class="form-horizontal" action="#">


@foreach($itemColumnsAndValues as $columnName => $columnValue)
    <div class="form-group">
        <label class="control-label col-sm-2">{{$columnName}} </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="pwd" value="{{$columnValue}}">

        </div>
    </div>

@endforeach
    <button type="submit" class="btn btn-default">Submit Item Changes</button>
</form>

@stop