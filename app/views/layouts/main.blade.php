<!DOCTYPE html>
<html>
<head>
    <title></title>
<!--    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">-->
    {{HTML::style('css/bootstrap.min.css')}}
<!--    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">-->
    {{HTML::style('css/font-awesome.min.css')}}
<!--    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>-->
<!--    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
    {{HTML::script('js/jquery-1.11.2.min.js')}}
<!--    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>-->
    {{HTML::script('js/bootstrap.min.js')}}
    {{ HTML::style('css/styles2.css') }}
<!--    {{ HTML::style('awesomplete/awesomplete.css') }}-->
<!--    {{ HTML::script('js/jquery.tablesorter.js') }}-->
    {{ HTML::style('css/dataTables.style.css')}}
    {{--<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>--}}
    {{HTML::script('js/dataTables.min.js')}}
    {{HTML::script('js/jquery.validate.js')}}
    {{HTML::script('js/jquery.jeditable.js')}}
    {{HTML::script('js/jquery.dataTables.editable.js')}}
    {{HTML::script('js/jquery-ui.min.js')}}
    {{HTML::script('js/naturalSort.js')}}
    {{--<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">--}}
    {{HTML::style('css/jquery.dataTables.css')}}



    <style>
        #sortable1, #sortable2, #sortable3 {
            border: 1px solid #eee;
            width: 142px;
            min-height: 20px;
            list-style-type: none;
            margin: 0;
            padding: 2px 0 0 0;
            float: left;
            margin-right: 10px;
        }
        #sortable1 li, #sortable2 li, #sortable3 li {
            margin: 0 5px 5px 5px;
            padding: 2px;
            font-size: 1em;
            width: 120px;
        }
        table {
            width: 100%;
            border: solid black 1px;
            border-collapse: collapse;
        }
        th, td {
            padding: 0.25em;
            border: solid black 1px;
        }

        th {
            background-color: #D2D2D2;
        }
    </style>
</head>
<body>

<div id="topleft">

    @if(Auth::check())
    <div id="topright">
      Welcome, {{Auth::user()->user_name}}  <a href="{{URL::to('/logout')}}">Logout</a>

    </div>
    @else
    <div id="topright">
<!--        <a href="{{URL::to('/login')}}">Login</a>-->
        {{Form::open(['route' => 'sessions.store'])}}
        Username: <input type="text" name="user_name"> Password: <input type="password" name="hashed_password"> <button type="submit"> Login</button>
        {{Form::close()}}
    </div>

    @endif
</div>
@include('navigation.navbar')

    <div class="container-fluid" id="content">
        <div class="side-body">
            @include('partials.messages')

            @yield('maincontent')
            <br>
        </div>
    </div>





{{--{{ HTML::script('js/jquery.js') }}--}}
{{ HTML::script('js/js.js') }}
<!--{{ HTML::script('awesomeplete/awesomeplete.js') }}-->
{{ HTML::script('js/jquery.nicescroll.js') }}

<script type="text/javascript">
    jQuery(function(){

    });
</script>
</body>
</html>