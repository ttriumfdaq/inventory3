<?php
/**
 * Created by PhpStorm.
 * User: Catherine
 * Date: 4/1/15
 * Time: 11:08 AM
 */


class ProfileController extends BaseController {

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */


    public function createNew(){
        $validator = Validator::make(Input::all(),
            array(
                'user_name' => 'required|unique:users|max:50',
                'hashed_password' => 'required|min:6|max:30',
                'cpassword' => 'required|same:hashed_password'
            )
        );

        if($validator->fails())
        {
            // redirect back to create-account with error message

            return Redirect::to('/root/createnew')->withErrors($validator)->withInput();
        }
        else
        {
            $user = new User;
            $user->user_name = Input::get('user_name');

            $user->hashed_password = Hash::make(Input::get('hashed_password'));

            $user->save();
            die($user->hashed_password);
            if($user)
            {
                echo "User has been added";
            }
            else
            {
                return 'Insert failed';
            }
        }
    }
    public function signIn()
    {
        //Posted from main.blade.php
        // How to implement http://laravel.com/docs/4.2/security
//        $auth = Auth::attempt(
//        array(
//            'user_name' => Input::get('username'), // Check email
//            'password' => Input::get('password')
//            )
//        );
//       echo Input::get('password');
        $username = Input::get('user_name');
        $password = Input::get('hashed_password');
        $hashed = Hash::make($password);

//        $user = User::where('user_name', '=', $username)->where('hashed_password', '=', $password)->first();
//        if($user){
//            Redirect::to('home');
//        } else {
//            echo "Login failed.";
//
//        }
        $credentials = array('user_name' => $username, 'hashed_password' => $password);
        if (Auth::attempt($credentials))
        {
            echo "You've logged in";
//            $username = Input::get('user_name');
//            $user = User::where('user_name', '=', $username);
//
//            //  return View::make('blog')->with('posts', $posts);
//            if ($user->count()){
//
//                $user = $user->first();
//
//               // return View::make('home')->with('user',$user);
//                echo "Logged in";
//            }
//            return "<p>Sorry, profile not found.</p>";
        }
        else
        {

            echo "Username: " . $username . "<br>";
            echo "Password: " . $password . "<br>";
            echo $hashed;
        }
    }

    public function editUser($id){
        echo $id;
    }

    public function deleteUser($id){
        echo $id;
    }

}

