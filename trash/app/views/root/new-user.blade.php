@extends('layouts.main')

@section('maincontent')

<h1>Create a new user</h1>

{{Form::open(array('url' => 'root/createnew'))}}

<div>
    {{Form::label('user_name', 'Username:')}} <br>
    {{Form::text('user_name')}}<br>

    @if($errors->has('user_name'))
    {{$errors->first('user_name')}}<br>
    @endif
</div>
<br>
<div>
    {{Form::label('hashed_password', 'Password:')}}<br>
    {{Form::password('hashed_password')}}<br>

    @if($errors->has('hashed_password'))
    {{$errors->first('hashed_password')}}<br>
    @endif
</div>
<br>
<div>
    {{Form::label('cpassword', 'Confirm password:')}}<br>
    {{Form::password('cpassword')}}<br>

    @if($errors->has('cpassword'))
    {{$errors->first('cpassword')}}<br>
    @endif
</div>
<br>
<div>
    {{Form::submit('Create New User')}}
</div>
{{Form::close()}}

@stop