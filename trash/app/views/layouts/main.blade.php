<!DOCTYPE html>
<html>
<head>
    <title></title>
    {{ HTML::style('css/styles.css'); }}
</head>
<body>

<div id="topleft">

    @if(Auth::check())
    @else
    <div id="topright">

        <form method="post" action="{{URL::route('post-sign-in')}}">
            <a href="{{URL::route('get-create-new-user')}}">Create new user</a>
            Username: <input name="user_name" type="text">
            Password: <input name="hashed_password" type="password">
            <input type="submit">
        </form>
    </div>

    @endif
</div>
<br>
<div id="content">
@yield('maincontent')
</div>
<div id="leftbar">
@include('navigation.navbar')
</div>
</body>
</html>